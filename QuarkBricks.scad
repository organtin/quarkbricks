use <fancybricks.scad> 

/*
    QuarkBricks
    Copyright (C) 2016  Giovanni Organtini (giovanni.organtini@roma1.infn.it)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* GENERAL */

module move(z1=0, t0=0, t1=0.3) {
    z0 = 80;
    v = (z1-z0)/(t1-t0);
    z = z0 + v * ($t - t0);
    translate ([0,0,z]) children(0);
}

module ell(clr="Red") {
  color(clr) {
    brick(1,2,1);
    brick(2,1,1);
  }
}

module base(clr="Red") {
  color(clr) {
    ell(clr);
    translate([32,0,0]) rotate([0,0,90]) ell(clr);
    translate([0,32,0]) rotate([0,0,-90]) ell(clr);
    translate([32,32,0]) rotate([0,0,180]) ell(clr);
  }
}

module minus(clr="Red", complete=true) {
  ell(clr);
  translate([0,32,0]) rotate([0,0,-90]) ell(clr);
  color(clr) {
    translate([16,0,0]) brick(2,1,1);
    translate([16,24,0]) brick(2,1,1);
    if (complete) {
      translate([24,8,0]) brick(1,2,1);
    } else {
      translate([24,8,12.5]) brick(1,2,1);      
    }
    translate([8,0,3.5]) brick(3,1,2.5);
    translate([0,0,3.5]) brick(1,3,2.5);
    translate([0,24,3.5]) brick(3,1,2.5);
    if (complete) {
      translate([24,8,3.5]) brick(1,3,2.5);
    } else {
      translate([24,24,3.5]) brick(1,1,2.5);
      translate([24,8,16]) brick(1,2,2.5);     
    }
  }  
}

module plus(clr="Red") {
  color(clr) {
    translate([8,8,0]) brick(2, 2, 1);
    translate([8,8,3.5]) brick(2, 2, 2.5);
  }
}

/* QUARKS */

module strange(clr="Red") {
  color(clr) {
    ell(clr);
    translate([0, 32, 0]) rotate([0,0,-90]) ell(clr);
    translate([16, 0, 0]) brick(2, 1, 1);
    translate([16, 24, 0]) brick(2, 1, 1);
    translate([0,0,3.5]) brick(4, 1, 2.5);
    translate([0,24,3.5]) brick(4, 1, 2.5);
    translate([0,8,3.5]) brick(1, 2, 2.5);
  }
}

module antistrange(clr="Red") {
  color(clr) {
    translate([8,8,0]) brick(3, 2, 1);
    translate([8,8,3.5]) brick(3, 2, 2.5);
  }
}

module up(clr="Red") {
  color(clr) {
    plus(clr);
    translate([0, 0, 12]) plus(clr);
  }
}

module antiup(clr="Red") {
  color(clr) {
    minus(clr);
    translate([0, 0, 12]) minus(clr);
  }
}

module down(clr="Red") {
  minus(clr);
}

module antidown(clr="Red") {
  plus(clr);
}

/* PARTICLES */

module Deltapp(clr="Red") {
  if ($t < 0.3) {
    move() up(clr);
  } else if ($t < 0.6) {
    up(clr);
    move(24, 0.3, 0.6) up(clr);
  } else {
    up(clr);
    translate([0,0,24]) up(clr);
    move(48, 0.6, 1-1./20) up(clr);
  }
}

module Deltap(clr="Red") {
  if ($t < 0.3) {
    move() down(clr);
  } else if ($t < 0.6) {
    down(clr);
    move(0, 0.3, 0.6) up(clr);
  } else {
    down(clr);
    translate([0,0,0]) up(clr);
    move(24, 0.6, 1-1./20) up(clr);
  }
}

module Delta0(clr="Red") {
  if ($t < 0.3) {
    move() down(clr);
  } else if ($t < 0.6) {
    down(clr);
    move(0, 0.3, 0.6) up(clr);
  } else {
    down(clr);
    translate([0,0,0]) up(clr);
    move(12, 0.6, 1-1./20) down(clr);
  }
}

module Deltam(clr="Red") {
  if ($t < 0.3) {
    move() down(clr);
  } else if ($t < 0.6) {
    down(clr);
    move(12, 0.3, 0.6) down(clr);
  } else {
    down(clr);
    translate([0,0,12]) down(clr);
    move(24, 0.6, 1-1./20) down(clr);
  }
}

module Sigmam(clr="Red") {
 if ($t < 0.3) {
    move() down(clr);
  } else if ($t < 0.6) {
    down(clr);
    move(12, 0.3, 0.6) down(clr);
  } else {
    down(clr);
    translate([0,0,12]) down(clr);
    move(24, 0.6, 1-1./20) strange(clr);
  }
}

module Sigma0(clr="Red") {
 if ($t < 0.3) {
    move() up(clr);
  } else if ($t < 0.6) {
    up(clr);
    move(0, 0.3, 0.6) down(clr);
  } else {
    up(clr);
    down(clr);
    move(12, 0.6, 1-1./20) strange(clr);
  }
}

module Sigmap(clr="Red") {
 if ($t < 0.3) {
    move() strange(clr);
  } else if ($t < 0.6) {
    strange(clr);
    move(0, 0.3, 0.6) up(clr);
  } else {
    strange(clr);
    up(clr);
    move(24, 0.6, 1-1./20) up(clr);
  }
}

module Csim(clr="Red") {
 if ($t < 0.3) {
    move() down(clr);
  } else if ($t < 0.6) {
    down(clr);
    move(12, 0.3, 0.6) strange(clr);
  } else {
    down(clr);
    translate([0,0,12]) strange(clr);
    move(24, 0.6, 1-1./20) strange(clr);
  }
}

module Csi0(clr="Red") {
 if ($t < 0.3) {
    move() up(clr);
  } else if ($t < 0.6) {
    up(clr);
    move(0, 0.3, 0.6) strange(clr);
  } else {
    up(clr);
    strange(clr);
    move(12, 0.6, 1-1./20) strange(clr);
  }
}

module Omegam(clr="Red") {
 if ($t < 0.3) {
    move() strange(clr);
  } else if ($t < 0.6) {
    strange(clr);
    move(0, 0.3, 0.6) strange(clr);
  } else {
    strange(clr);
    translate([0, 0, 12]) strange(clr);
    move(24, 0.6, 1-1./20) strange(clr);
  }
}

module proton(clr="Red") {
  Deltap(clr);
}

module neutron(clr="Red") {
  Deltam(clr);
}

module Lambda(clr="Red") {
  Sigma0(clr);
}

module pi0(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) up();
  } else {
    up();
    move(t1=0.5, t1=1-1./20) antiup();
  }
}

module eta(clr="Red") {
   if ($t < 0.5) {
       move(t0=0, t1=0.5, z1=0) antistrange();
   } else {
       antistrange();
       move(t0=0.5, t1=1.-1./20, z1=0) strange();
   }
}

module pip(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) up();
  } else {
    up();
    move(z1=24, t0=0.5, t1=1-1./20) antidown();
  }
}

module pim(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) down();
  } else {
    down();
    move(z1=12, t0=0.5, t1=1-1./20) antiup();
  }
}

module antiK0(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) strange();
  } else {
    strange();
    move(z1=0, t0=0.5, t1=1-1./20) antidown();
  }
}

module Km(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) antiup();
  } else {
    antiup();
    move(z1=24, t0=0.5, t1=1-1./20) strange();
  }
}

module Kp(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) antistrange();
  } else {
    antistrange();
    move(z1=18, t0=0.5, t1=1-1./20) up();
  }
}

module K0(clr="Red") {
  if ($t < 0.5) {
    move(t1=0.5) antistrange();
  } else if ($t <= 0.94) {
    antistrange();
    move(z1=12, t0=0.5, t1=0.94) down();
  } else {
    antistrange();
    move(z1=0, t0=0.5, t1=1-1./20) minus(complete=false);
  }
}

/* EXAMPLES */

{
  translate([-16,-16,0]) {
      // this is an Sigma0 or a Lambda0 with coloured quarks 
      $t=1-1./20;
      down();
      translate([0,0,12]) strange("blue");
      up("green");
  }
  translate([-16,-64,0]) {
     // an antiK0
     $t=1-1./20; 
     antiK0();
  }
  translate([-16, 32, 0]) {
     // just the base of a unit
     $t=1-1./20; 
     base();
  }
}
